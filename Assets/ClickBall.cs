﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickBall : MonoBehaviour
{
    public Transform Player;

    public GameObject Go_Ball;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        RaycastHit hit =new RaycastHit();
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.GetComponent<CollisionDetection>())
                {
                    hit.collider.GetComponent<Rigidbody>().AddForce(Vector3.up * 6, ForceMode.Impulse);
                }
            }
        }
    }
}
