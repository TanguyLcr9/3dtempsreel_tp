﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blocker : MonoBehaviour
{
    public GameObject Go_BlockerObject;

    private void Start()
    {
        Go_BlockerObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Ball"))
        {
            GameManagerFlipper.Instance.B_BallIsInside = true;
            Go_BlockerObject.SetActive(true);
        }
    }

    
}
