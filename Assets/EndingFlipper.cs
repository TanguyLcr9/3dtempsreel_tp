﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndingFlipper : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Ball") && GameManagerFlipper.Instance.B_BallIsInside) { 
            
                StartCoroutine(Retry());
            
        }

    }

    IEnumerator Retry()
    {
        GameManagerFlipper.Instance.B_BallIsInside = false;
        yield return new WaitForSeconds(2f);
        GameManagerFlipper.Instance.Restart();
        
    }
}
