﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using System.Runtime.Serialization.Formatters.Binary;//encodage binaire
using System.IO;//gestion des streams

[System.Serializable]
public class Student
{
    public string name;
    public int age;
    public float height;

}
public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public int level;

    public List<int> Count = new List<int>();

    public TextMeshProUGUI Count_Text;

    public Student student = new Student(); 
    // Start is called before the first frame update
    void Awake()
    {
        if (Instance != null)
        {
            Destroy(this.gameObject);
        }
        Instance = this;
        DontDestroyOnLoad(this.gameObject);

    }
    void Start()
    {
        AddCount();

        /*MemoryStream stream = new MemoryStream();
        BinaryFormatter encoder = new BinaryFormatter();
        encoder.Serialize(stream, student);
        Debug.Log(stream.Length);
        stream.Close();*/

       
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Submit")) { level++; SceneManager.LoadScene(level); }
    }

    public void AddCount(int incrementation = 0, int type = 0)
    {
        Count[type] += incrementation;
        string text="";
       for(int i=0;i<Count.Count;i++)
        {
            if (i == Count.Count - 1)
            {
                text += Count[i];
            }
            else
            {
                text += Count[i] + " / ";
            }
        }
        Count_Text.text = text;
    }

}