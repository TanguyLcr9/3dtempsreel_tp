﻿using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;//encodage binaire
using System.IO;//gestion des streams

[System.Serializable]
public class SaveData
{
    public float posX = 0.0f;
    public float posY = 0.0f;
}

public class CanvasScript : MonoBehaviour
{
    public RectTransform Image;
    public float speed = 1;
    // Start is called before the first frame update
    void Start()
    {
        float X=0, Y=0;
        //X = PlayerPrefs.GetFloat("positionX");
        //Y = PlayerPrefs.GetFloat("positionY");
        try {
            if (File.Exists(Application.persistentDataPath + "/save.dat"))
            {
                BinaryFormatter decoder = new BinaryFormatter();
                FileStream stream = File.OpenRead(Application.persistentDataPath + "/save.dat");
                SaveData data = (SaveData)decoder.Deserialize(stream);
                X = data.posX;
                Y = data.posY;
                stream.Close();
            }
        } catch (System.Exception)
        {

        }
        
        Image.position = new Vector3(X, Y, 0.0f);
    }

    // Update is called once per frame
    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        Image.position += new Vector3(horizontal, vertical).normalized * speed;
        if (Input.GetKeyDown(KeyCode.M))
        {
            //PlayerPrefs.SetFloat("positionX", Image.position.x);
            //PlayerPrefs.SetFloat("positionY", Image.position.y);
            Save();
        }

    }

    void Save()
    {
        SaveData data = new SaveData();
        data.posX = Image.position.x;
        data.posY = Image.position.y;

        BinaryFormatter encoder = new BinaryFormatter();
        Debug.Log(Application.persistentDataPath + "/save.dat");
        FileStream stream = File.OpenWrite(Application.persistentDataPath + "/save.dat");
        encoder.Serialize(stream, data);
        stream.Close();
    }


}
