﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thunder : MonoBehaviour
{
    public float F_MiniWait = 10f, F_EcartWait = 30f;
    public AudioClip Thunder_Clip;

    Animator animator;
    AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        StartCoroutine(WaitForThunder());
        audioSource = GetComponent<AudioSource>();
    }

    IEnumerator WaitForThunder()
    {
        while (true)// On souhaite que la boucle se joue indéfiniment
        {
            // Par sécurite : Au cas ou on rentre des chiffres trop faibles risquant de faire répéter la boucle trop souvent à cause du while(true)
            if (F_MiniWait<0.1f && F_EcartWait < 0.1f) { Debug.LogError("Thunder broke down : changes the variables"); break; }

            //Temps aléatoire entre un temps minimum et le temps minimum plus un écart défini
            float ranSeconds = Random.Range(F_MiniWait, F_MiniWait + F_EcartWait);

            //Attente
            yield return new WaitForSeconds(ranSeconds);

            //Jouer le son de l'orage
            audioSource.clip = Thunder_Clip;
            audioSource.Play();

            //Jouer l'animation de l'orage
            animator.Play("Eclair_Anim");
        }
    }
}
