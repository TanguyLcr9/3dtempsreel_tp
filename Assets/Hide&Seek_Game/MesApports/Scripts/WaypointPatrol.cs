﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WaypointPatrol : MonoBehaviour
{
    public NavMeshAgent navMeshAgent;
    public Transform[] waypoints;
    public float F_DetectionDistance;

    int m_CurrentWaypointIndex;
    Transform T_positionDuJoueur;

    void Start ()
    {
        navMeshAgent.SetDestination (waypoints[0].position);
        T_positionDuJoueur = PlayerMovement.Instance.transform;
    }

    void Update ()
    {
        //la position du joueur en priorité, si elle est inférieur au seuil défini, lui même multiplié par la vitesse du joueur si celui -ci cours, il devient plus facile à repérer pour les fantomes
        if (Vector3.Distance(T_positionDuJoueur.position, transform.position) < F_DetectionDistance * PlayerMovement.Instance.speed)
        {
            navMeshAgent.SetDestination(T_positionDuJoueur.position);// tout simplement prendre le joueur en target grace au navmeshagent
        }
        else
        {
            if (navMeshAgent.remainingDistance < navMeshAgent.stoppingDistance)//
            {
                m_CurrentWaypointIndex = (m_CurrentWaypointIndex + 1) % waypoints.Length;
                navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position);
            }
        }
        navMeshAgent.speed = 1.5f + PlayerMovement.Instance.Count * 0.03f;//Augmentation de la vitesse des agents fantomes en fonction du nombre de coins récupérer par le joueur
    }
}
