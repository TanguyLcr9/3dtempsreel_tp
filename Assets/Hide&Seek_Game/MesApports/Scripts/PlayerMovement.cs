﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using TMPro;


public class PlayerMovement : MonoBehaviour
{
    public static PlayerMovement Instance;
    public float turnSpeed = 20f;
    public float speed = 1;
    public int Count=0;
    public int CountGoal = 1;
    public TextMeshProUGUI Count_TextMeshPro;

    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;
    public NavMeshAgent navMesh;

    private void Awake()
    {
        if(Instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    void Start ()
    {
        m_Animator = GetComponent<Animator> ();
        m_Rigidbody = GetComponent<Rigidbody> ();
        m_AudioSource = GetComponent<AudioSource> ();
        navMesh = gameObject.AddComponent<NavMeshAgent>();
        UpdateCountText();
    }

    void FixedUpdate ()
    {
        float horizontal = Input.GetAxis ("Horizontal");
        float vertical = Input.GetAxis ("Vertical");
        
        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize ();

        bool hasHorizontalInput = !Mathf.Approximately (horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately (vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool ("IsWalking", isWalking);
        

        speed = Input.GetKey(KeyCode.LeftShift) ? 1.5f:1f; //usage d'un ternaire afin de changer la vitesse du personnage ainse le faire courrir
        m_Animator.speed = speed;// accélère aussi l'animation

        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop ();
        }

        Vector3 desiredForward = Vector3.RotateTowards (transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation (desiredForward);
    }

    void OnAnimatorMove ()
    {
        m_Rigidbody.MovePosition (m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude * speed);
        m_Rigidbody.MoveRotation (m_Rotation);
    }

    private void OnTriggerEnter(Collider other)
    {

            if(other.gameObject.CompareTag("Coin"))//Si l'objet entrant en collision est un coin alors
        {
            Count++;//ajouter +1 à Count
            Destroy(other.gameObject);//le détruire
            UpdateCountText();//mettre à jour l'affichage HUD
            if (Count >= CountGoal)//si on a récuperer tout les coins
            {
                GameEnding.Instance.m_IsPlayerAtExit = true;//alors afficher la fin du jeu
            }
        }
    }

    /// <summary>
    /// Met à jour le texte en HUD
    /// </summary>
    private void UpdateCountText()
    {
        Count_TextMeshPro.text = "COUNT : " + Count;
    }
}