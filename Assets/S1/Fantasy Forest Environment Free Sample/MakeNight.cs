﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeNight : MonoBehaviour
{
    public Material M_Skybox;
    public Light Sun_Light;
    public Transform T_Light;
    public float F_Intensity;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        F_Intensity = (float)Input.mousePosition.x / Screen.width;// entre 0 et 1, 0 si la souris est le plus à gauche et 1 si la souris est le plus à droite
        
        M_Skybox.SetFloat("_Exposure", Mathf.Lerp(0,5,F_Intensity));
        Sun_Light.intensity = Mathf.Lerp(0.2f, 4f, F_Intensity);
        T_Light.eulerAngles = new Vector3(Mathf.Lerp(0, 90, F_Intensity), 0, 0);

        F_Intensity = (float)Input.mousePosition.y / Screen.height;

        M_Skybox.SetFloat("_AtmosphereThickness", Mathf.Lerp(1, 5, F_Intensity));
        Sun_Light.color = new Color(1, Mathf.Lerp(1, 0, F_Intensity), Mathf.Lerp(1,0, F_Intensity));

    }
}
