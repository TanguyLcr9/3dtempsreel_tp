﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementSphere : MonoBehaviour
{
    Vector3 movement;
    Rigidbody rb;
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        movement = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")) *speed;
        /*if (transform.position.y <= -10)
        {
            transform.position = Vector3.zero;
        }*/
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rb.AddForce(movement, ForceMode.Force);
        if (Input.GetButtonDown("Jump"))
        {
            rb.AddForce(Vector3.up * 200, ForceMode.Force);
        }


    }
}
