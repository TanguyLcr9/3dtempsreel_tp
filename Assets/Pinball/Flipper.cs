﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flipper : MonoBehaviour
{
    public bool isLeft;
    Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        //  transform.eulerAngles += ((Input.GetButton("Fire1") ? angleReach.eulerAngles : angleBase.eulerAngles) -transform.eulerAngles) *0.3f;
        //transform.localEulerAngles += ((Input.GetButton("Fire1") ? angleReach.localEulerAngles : angleBase.localEulerAngles) - transform.localEulerAngles) * 0.3f;
        if (Input.GetMouseButton(isLeft?0:1) || Input.GetKey(isLeft?KeyCode.LeftArrow:KeyCode.RightArrow) )
        {
            animator.SetBool("Shoot",true);
        }else
        {
            animator.SetBool("Shoot",false);
        }
    }
}
