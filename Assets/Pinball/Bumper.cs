﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Bumper : MonoBehaviour
{
    public static float strenght = 1000;
    Tween myTween;
    Tween myTween_2;
    Color colorBase;

    private void Start()
    {
        colorBase = GetComponent<MeshRenderer>().material.GetColor("_EmissionColor");
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ball"))
        {
            
            StartCoroutine(DoBump());

            Rigidbody Rb = collision.gameObject.GetComponent<Rigidbody>();
            //Rb.AddExplosionForce(strenght,collision.transform.position,5);

        }
    }

    IEnumerator DoBump()
    {
        float duration = 0.05f;
        if(myTween != null)
        {
            myTween.Kill();
            myTween_2.Kill();

            transform.localScale = new Vector3(1, 1, 1);
            GetComponent<MeshRenderer>().material.SetColor("_EmissionColor",colorBase);
        }

        myTween = GetComponent<Rigidbody>().transform.DOScale(1.2f, duration).SetLoops(2,LoopType.Yoyo);
        myTween_2 = GetComponent<MeshRenderer>().material.DOColor(Color.white, "_EmissionColor", 0.1f).SetLoops(2, LoopType.Yoyo);

        yield return myTween.WaitForCompletion();


    }
}
