﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameManagerFlipper : MonoBehaviour
{
    public static GameManagerFlipper Instance;

    public GameObject Go_Ball;
    public Blocker Blocker_script;
    public Transform T_Spawner;
    public Slider Boost_Slider;
    public bool B_BallIsInside;
    public int Life = 3;

    GameObject go_activeBall;
    bool B_OnStart = true;
    bool B_BoostUp=true;
    // Start is called before the first frame update

    private void Awake()
    {
        if(Instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    void Start()
    {
        StartCoroutine(StartBall());
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(0);
        }
        
        
        if (B_OnStart && go_activeBall.GetComponent<Rigidbody>().velocity.magnitude <= 0.001f) {

            if (Input.GetKeyDown(KeyCode.Space))
            {
                Boost_Slider.value = Boost_Slider.minValue;
                B_BoostUp = true;
            }else

            if (Input.GetKey(KeyCode.Space)) {
                Boost_Slider.value += (B_BoostUp ? Time.deltaTime : -Time.deltaTime) * 3f;
                if (Boost_Slider.value >= Boost_Slider.maxValue)
                {
                    Boost_Slider.value = Boost_Slider.maxValue;
                    B_BoostUp = false;
                }
                if (Boost_Slider.value <= Boost_Slider.minValue)
                {
                    Boost_Slider.value = Boost_Slider.minValue;
                    B_BoostUp = true;
                }
            }else


            if (Input.GetKeyUp(KeyCode.Space))
            {
                Debug.Log("SHOOOT");
                go_activeBall.GetComponent<Rigidbody>().AddForce(Vector3.forward * 130 * Boost_Slider.value, ForceMode.Impulse);
                Boost_Slider.value = Boost_Slider.minValue;
            }
        }

    }

    public void Restart()
    {
        Blocker_script.Go_BlockerObject.SetActive(false);
        StartCoroutine(StartBall());
        Life--;

    }

    public IEnumerator StartBall() {

        //1 : la balle dans la goutiere spawn et tombe
        if(go_activeBall != null)
        Destroy(go_activeBall);
        go_activeBall = Instantiate(Go_Ball, T_Spawner);
        //yield return new WaitForSeconds(2f);

        ////2 : le joueur peux lancer la balle dans la goutiere en controlant la puissance
        B_OnStart = true;

        yield return new WaitUntil(()=>B_BallIsInside);
        B_OnStart = false;
    }
}
